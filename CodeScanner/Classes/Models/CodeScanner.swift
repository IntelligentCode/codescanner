//
//  BarcodeScanner.swift
//  CodeScanner
//
//  Created by Артем Шляхтин on 29/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import AVFoundation

@objc protocol CodeScannerDelegate: NSObjectProtocol {
    func scannerDidFoundCode(_ code: String)
    func scannerDidFailWithError(_ error: String?)
    func scannerUpdatePreview(_ layer: CALayer)
    
    @objc optional func scannerFrame() -> CGRect
    @objc optional func scannerCadre() -> CGRect
}

class CodeScanner: NSObject {
    
    weak var delegate: CodeScannerDelegate?
    
    fileprivate var captureSession: AVCaptureSession? = AVCaptureSession()
    fileprivate var metadataOutput: AVCaptureMetadataOutput!
    fileprivate let sessionQueue = DispatchQueue(label: "session queue", attributes: [])
    fileprivate var previewLayer: AVCaptureVideoPreviewLayer! {
        willSet {
            previewLayer?.removeFromSuperlayer()
        }
        
        didSet {
            delegate?.scannerUpdatePreview(previewLayer)
        }
    }
    
    
    // MARK: - Lifecycle
    
    override init() {
        super.init()
        performInitializations()
    }
    
    func layerWillTransition(toFrame frame: CGRect) {
        previewLayer.frame = frame
        changeVideoOrientation()
        updateCadre()
    }
    
    
    // MARK: - Actions
    
    func start() {
        sessionQueue.async {
            self.captureSession?.startRunning()
        }
    }
    
    func stop() {
        sessionQueue.async {
            self.captureSession?.stopRunning()
        }
    }
    
    func refresh() {
        sessionQueue.async {
            self.captureSession?.stopRunning()
            self.captureSession?.startRunning()
        }
    }
    
    func detectedCode(_ code: String) {
        DispatchQueue.main.async {
            self.delegate?.scannerDidFoundCode(code)
        }
    }
    
    func failed(_ desc: String?) {
        DispatchQueue.main.async {
            self.captureSession = nil
            self.delegate?.scannerDidFailWithError(desc)
        }
    }
    
    
    // MARK: - Layer
    
    func createPreviewLayer() {
        var newFrame = UIScreen.main.bounds
        if let userFrame = self.delegate?.scannerFrame?() {
            newFrame = userFrame
        }
        
        let layer = AVCaptureVideoPreviewLayer(session: captureSession)
        layer?.frame = newFrame
        layer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewLayer = layer
        changeVideoOrientation()
        updateCadre()
    }
    
    
    // MARK: - Private
    
    fileprivate func performInitializations() {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if cameraAuthorizationStatus != .authorized { return }
        
        sessionQueue.async {
            do {
                self.captureSession?.beginConfiguration()
                try self.prepareDevice()
                try self.prepareOutput()
                self.captureSession?.commitConfiguration()
            } catch {
                self.failed("Ошибка инициализации AVCapture")
            }
        }
    }
    
    fileprivate func prepareDevice() throws {
        let videoCaptureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        let videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        
        if (captureSession!.canAddInput(videoInput)) {
            captureSession?.addInput(videoInput)
        } else {
            throw NSError(domain: CodeScannerFramework.bundleName, code: 1001, userInfo: nil)
        }
    }
    
    fileprivate func prepareOutput() throws {
        metadataOutput = AVCaptureMetadataOutput()
        if (captureSession!.canAddOutput(metadataOutput)) {
            captureSession!.addOutput(metadataOutput)
            metadataOutput.setMetadataObjectsDelegate(self, queue: sessionQueue)
            metadataOutput.metadataObjectTypes = metadataObjectTypes()
        } else {
            throw NSError(domain: CodeScannerFramework.bundleName, code: 1002, userInfo: nil)
        }
    }
    
    fileprivate func changeVideoOrientation() {
        let orientation = UIDevice.current.orientation
        let connection = previewLayer.connection
        switch orientation {
            case .portrait:           connection?.videoOrientation = .portrait
            case .portraitUpsideDown: connection?.videoOrientation = .portrait
            case .landscapeLeft:      connection?.videoOrientation = .landscapeRight
            case .landscapeRight:     connection?.videoOrientation = .landscapeLeft
            default:
                connection?.videoOrientation = .portrait
        }
    }
    
    fileprivate func updateCadre() {
        sessionQueue.async {
            guard let cadre = self.delegate?.scannerCadre?() else { return }
            if self.metadataOutput == nil { return }
            self.metadataOutput.rectOfInterest = self.previewLayer.metadataOutputRectOfInterest(for: cadre)
        }
    }
    
    fileprivate func metadataObjectTypes() -> [AnyObject] {
        let barCodeTypes = [AVMetadataObjectTypeUPCECode,
                            AVMetadataObjectTypeCode39Code,
                            AVMetadataObjectTypeCode39Mod43Code,
                            AVMetadataObjectTypeEAN13Code,
                            AVMetadataObjectTypeEAN8Code,
                            AVMetadataObjectTypeCode93Code,
                            AVMetadataObjectTypeCode128Code,
                            AVMetadataObjectTypePDF417Code,
                            AVMetadataObjectTypeQRCode,
                            AVMetadataObjectTypeAztecCode
        ]
        return barCodeTypes as [AnyObject]
    }
    
}


extension CodeScanner: AVCaptureMetadataOutputObjectsDelegate {
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        captureSession?.stopRunning()
        
        if let readableObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            detectedCode(readableObject.stringValue)
        }
    }
    
}
