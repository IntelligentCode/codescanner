//
//  CadreConstructor.swift
//  CodeScanner
//
//  Created by Артём Шляхтин on 21.05.16.
//  Copyright © 2016 Артем Шляхтин. All rights reserved.
//

import UIKit

@objc protocol CadreConstructorDelegate: NSObjectProtocol {
    func cadreConstructorFrame() -> CGRect
    func cadreConstructorUpdate(_ layer: CALayer)
    func cadreConstructorShowMask(_ constructor: CadreConstructor) -> Bool
    
    @objc optional func cadreConstructorInteractiveArea(_ constructor: CadreConstructor) -> CGRect
    @objc optional func cadreConstructorInteractiveAreaBorder(_ constructor: CadreConstructor) -> CGFloat
    @objc optional func cadreConstructorInteractiveAreaColor(_ constructor: CadreConstructor) -> UIColor
    @objc optional func cadreConstructorShowRedLine(_ constructor: CadreConstructor) -> Bool
    @objc optional func cadreConstructorRedLineHeight(_ constructor: CadreConstructor) -> CGFloat
}

class CadreConstructor: NSObject {
    
    weak var delegate: CadreConstructorDelegate?
    fileprivate var cadreLayer: CAShapeLayer! {
        willSet { cadreLayer?.removeFromSuperlayer() }
        didSet {
            delegate?.cadreConstructorUpdate(cadreLayer)
        }
    }
    
    
    // MARK: - Lifecycle
    
    func layerWillTransition(toFrame frame: CGRect) {
        cadreLayer.sublayers?.removeAll()
        updatePath(frame)
        createCadreBorderLayer()
        addRedLine()
    }
    
    
    // MARK: - Actions
    
    func createCadre() {
        if stateMask() == false { return }
        createMaskLayer()
        createCadreBorderLayer()
        addRedLine()
    }
    
    
    // MARK: - Layer
    
    fileprivate func createMaskLayer() {
        let maskLayer = CAShapeLayer()
        maskLayer.fillColor = UIColor(white: 0.15, alpha: 0.6).cgColor
        maskLayer.fillRule = kCAFillRuleEvenOdd
        cadreLayer = maskLayer
        
        if let frame = delegate?.cadreConstructorFrame() {
            updatePath(frame)
        }
    }
    
    fileprivate func updatePath(_ frame: CGRect) {
        let path = UIBezierPath(rect: frame)
        path.append(UIBezierPath(rect: cadre))
        path.close()
        cadreLayer.path = path.cgPath
    }
    
    fileprivate func createCadreBorderLayer() {
        guard let borderWidth = delegate?.cadreConstructorInteractiveAreaBorder?(self) else { return }
        
        let lines = UIBezierPath()
        
        let lenghtStroke: CGFloat = 24.0
        let left = cadre.origin.x
        let top = cadre.origin.y
        let bottom = cadre.size.height + top
        let right = cadre.size.width + left
        
        lines.move(to: CGPoint(x: left, y: bottom - lenghtStroke))
        lines.addLine(to: CGPoint(x: left, y: bottom))
        lines.addLine(to: CGPoint(x: left + lenghtStroke, y: bottom))
        
        lines.move(to: CGPoint(x: left, y: top + lenghtStroke))
        lines.addLine(to: CGPoint(x: left, y: top))
        lines.addLine(to: CGPoint(x: left + lenghtStroke, y: top))
        
        lines.move(to: CGPoint(x: right - lenghtStroke, y: top))
        lines.addLine(to: CGPoint(x: right, y: top))
        lines.addLine(to: CGPoint(x: right, y: top + lenghtStroke))
        
        lines.move(to: CGPoint(x: right, y: bottom - lenghtStroke))
        lines.addLine(to: CGPoint(x: right, y: bottom))
        lines.addLine(to: CGPoint(x: right - lenghtStroke, y: bottom))
        
        let cornersLayer = CAShapeLayer()
        cornersLayer.path = lines.cgPath
        cornersLayer.strokeColor = delegate?.cadreConstructorInteractiveAreaColor?(self).cgColor
        cornersLayer.lineWidth = borderWidth
        cornersLayer.fillColor = UIColor.clear.cgColor
        
        cadreLayer.addSublayer(cornersLayer)
    }
    
    fileprivate func addRedLine() {
        guard let isShowRedLine = delegate?.cadreConstructorShowRedLine?(self), isShowRedLine == true else { return }
        guard let lineHeight = delegate?.cadreConstructorRedLineHeight?(self) else { return }
        
        let top = cadre.origin.y
        let left = cadre.origin.x
        let right = cadre.size.width + left
        let center = (cadre.size.height / 2.0) + top
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: left, y: center))
        path.addLine(to: CGPoint(x: right, y: center))
        
        let line = CAShapeLayer()
        line.path = path.cgPath
        line.strokeColor = UIColor.red.cgColor
        line.lineWidth = lineHeight
    
        cadreLayer.addSublayer(line)
    }
    
    
    // MARK: - Properties
    
    var cadre: CGRect {
        get { return calculateCadre() }
    }
    
    fileprivate func calculateCadre() -> CGRect {
        guard let frame = delegate?.cadreConstructorInteractiveArea?(self) else { return CGRect.zero }
        return frame
    }
    
    fileprivate func stateMask() -> Bool {
        guard let state = delegate?.cadreConstructorShowMask(self) else { return false }
        return state
    }
    
    
    // MARK: - Settings
    
    fileprivate func detectOrientation() -> UIInterfaceOrientation {
        let deviceOrientation = UIDevice.current.orientation
        var orientation: UIInterfaceOrientation
        switch (deviceOrientation) {
            case .portrait:           orientation = .portrait
            case .portraitUpsideDown: orientation = .portraitUpsideDown
            case .landscapeLeft:      orientation = .landscapeLeft
            case .landscapeRight:     orientation = .landscapeRight
            default:
                orientation = .portrait
        }
        return orientation
    }

}
