//
//  Library.swift
//  CodeScanner
//
//  Created by Артём Шляхтин on 26/01/2017.
//  Copyright © 2017 Artem Shlyakhtin. All rights reserved.
//

import Foundation

public struct CodeScannerFramework {
    public static let bundleName = "ru.roseurobank.CodeScanner"
}
