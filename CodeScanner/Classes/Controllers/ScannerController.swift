//
//  BarcodeScannerController.swift
//  CodeScanner
//
//  Created by Артем Шляхтин on 29/09/15.
//  Copyright © 2015 Артем Шляхтин. All rights reserved.
//

import UIKit
import AVFoundation

@objc
public protocol ScannerDelegate: NSObjectProtocol {
    
    /**
     Событие срабатывает в том случае, если удалось распознать код.
     
     - parameter code: Код, который удалось распознать.
    */
    func scanner(didFoundCode code: String)
    
    /**
     Событие срабатывает в том случае, если возникла ошибка.
     
     - parameter error: Описание возникшей ошибки.
     */
    func scanner(didFailWithError error: String?)
    
    /** Задает настройки отображения маски и рамки. */
    @objc optional func scannerShowMask() -> Bool
    
    /** Задает настройки ширины рамки. */
    @objc optional func scannerCadreBorderHeight() -> CGFloat
    
    /** Задает настройки цвета рамки. */
    @objc optional func scannerCadreColor() -> UIColor
    
    /** Задает настройки отображения красной линии. */
    @objc optional func scannerShowRedLine() -> Bool
    
    /** Задает настройки ширины красной рамки */
    @objc optional func scannerRedLineHeight() -> CGFloat
}

open class ScannerController: UIViewController {
    
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    open weak var delegate: ScannerDelegate?
    fileprivate var cadreConstructor = CadreConstructor()
    fileprivate let codeScanner = CodeScanner()
    fileprivate var interactiveArea = CGRect.zero
    
    
    // MARK: - Lifecycle
    
    open override func loadView() {
        super.loadView()

        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
        if cameraAuthorizationStatus != .authorized {
            self.desc.text = "Включите в найстройках возможность взаимодействовать с вашей камерой"
            self.desc.textColor = UIColor.gray
            self.view.backgroundColor = UIColor.groupTableViewBackground
            self.closeButton.tintColor = UIButton(type: .system).titleColor(for: .normal)
            return
        }
        
        interactiveArea = calculateCadre(calculateBounds)
        
        cadreConstructor.delegate = self
        cadreConstructor.createCadre()
        
        codeScanner.delegate = self
        codeScanner.createPreviewLayer()
        codeScanner.start()
    }

    open override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    open override var prefersStatusBarHidden : Bool {
        return true
    }
    
    open override var shouldAutorotate : Bool {
        return true
    }
    
    open override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.allButUpsideDown
    }

    
    // MARK: - Actions

    @IBAction func close(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: - Rotate
    
    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        let newFrame = CGRect(origin: CGPoint.zero, size: size)
        interactiveArea = calculateCadre(newFrame)
        cadreConstructor.layerWillTransition(toFrame: newFrame)
        codeScanner.layerWillTransition(toFrame: newFrame)
    }
    
    // MARK: - Settings
    
    fileprivate func calculateCadre(_ newViewFrame: CGRect) -> CGRect {
        var result = CGRect.zero
        let orientation = UIDevice.current.orientation
        switch orientation {
            case .landscapeLeft, .landscapeRight:
                let height = newViewFrame.height * 0.48
                let top = ((newViewFrame.height / 2.0) - (height / 2.0)) * 0.7
                let width = newViewFrame.width * 0.9
                let left = (newViewFrame.width / 2.0) - (width / 2.0)
                result = CGRect(x: left, y: top, width: width, height: height)
            
            default:
                let width = newViewFrame.width * 0.9
                let height = width
                let top = ((newViewFrame.height / 2.0) - (height / 2.0)) * 0.7
                let left = (newViewFrame.width / 2.0) - (width / 2.0)
                result = CGRect(x: left, y: top, width: width, height: height)
        }
        
        return result
    }
    
    // MARK: - Properties
    
    var calculateBounds: CGRect {
        get {
            let orientation = UIDevice.current.orientation
            let isLandscape = (orientation == .landscapeLeft || orientation == .landscapeRight) ? true : false
            let isHeightGreater = (view.bounds.height > view.bounds.width) ? true : false
            if isLandscape && isHeightGreater {
                //Если родительское представление было заблокировано
                return CGRect(x: 0, y: 0, width: view.bounds.height, height: view.bounds.width)
            }
            return view.bounds
        }
    }
    
}


// MARK: - Scanner View Layer Delegate

extension ScannerController: CadreConstructorDelegate {
    
    func cadreConstructorFrame() -> CGRect {
        return calculateBounds
    }
    
    func cadreConstructorUpdate(_ layer: CALayer) {
        view.layer.insertSublayer(layer, at: 0)
    }
    
    func cadreConstructorShowMask(_ constructor: CadreConstructor) -> Bool {
        guard let state = self.delegate?.scannerShowMask?() else { return true }
        return state
    }
    
    func cadreConstructorInteractiveArea(_ constructor: CadreConstructor) -> CGRect {
        return interactiveArea
    }
    
    func cadreConstructorInteractiveAreaBorder(_ constructor: CadreConstructor) -> CGFloat {
        guard let height = self.delegate?.scannerCadreBorderHeight?() else { return 3.0 }
        return height
    }
    
    func cadreConstructorInteractiveAreaColor(_ constructor: CadreConstructor) -> UIColor {
        guard let color = self.delegate?.scannerCadreColor?() else { return UIColor.yellow }
        return color
    }
    
    func cadreConstructorShowRedLine(_ constructor: CadreConstructor) -> Bool {
        guard let state = self.delegate?.scannerShowRedLine?() else { return true }
        return state
    }
    
    func cadreConstructorRedLineHeight(_ constructor: CadreConstructor) -> CGFloat {
        guard let height = self.delegate?.scannerRedLineHeight?() else { return 1.0 }
        return height
    }
    
}

// MARK: - Scanner Delegate

extension ScannerController: CodeScannerDelegate {
    
    func scannerDidFoundCode(_ code: String) {
        self.delegate?.scanner(didFoundCode: code)
        self.dismiss(animated: true, completion: nil)
    }
    
    func scannerDidFailWithError(_ error: String?) {
        self.delegate?.scanner(didFailWithError: error)
        self.dismiss(animated: true, completion: nil)
    }
    
    func scannerUpdatePreview(_ layer: CALayer) {
        view.layer.insertSublayer(layer, at: 0)
    }
    
    func scannerFrame() -> CGRect {
        return calculateBounds
    }
    
    func scannerCadre() -> CGRect {
        return interactiveArea
    }
    
}
